$(document).foundation({
  equalizer : {
    // Specify if Equalizer should make elements equal height once they become stacked.
    equalize_on_stack: true
  }
});
///////////////////////////////
var lang=$('html').attr("lang"),
upToDateSelfcare='';



$(document).ready(function(e) {
	
	
	/*var step=0;
		$(".spin").click(function(e) {		
		
		
			var dir=$(this).attr('data-dir'),
				digit=$(this).attr('data-digit');
			
			
			if(dir=='up'){
				step=(step==9?0:++step);
				$('#digit'+digit+' div').removeClass('up');
				$('#digit'+digit+' div').removeClass('down');
				$('#digit'+digit+' div:eq('+ step+')').addClass('up');
			}
			else{
				$('#digit'+digit+' div').css({'top':'100%'});
				step=(step>=0?--step:9);
				$('#digit'+digit+' div').removeClass('down');
				$('#digit'+digit+' div').removeClass('up');
				$('#digit'+digit+' div:eq('+ step+')').addClass('down');
				
			}
			console.log('step='+step);
			/*$(".digit div.active + div").addClass('up');
			setTimeout(function(){
			$(".digit div.active").removeClass('active');
			$(".digit div.up").addClass('active').removeClass('up');	
				
			},300);
		});
	*/
	///////
	
	
	
	/***********************/
	
	
	
	var step=[0,0,0,0,0,0,0];
		$(".spin").click(function(e) {
		$(".digit").removeClass("activated");
		$(this).parents(".digit-container").find(".digit").addClass("activated");
		
			var dir=$(this).attr('data-dir'),
				digit=$(this).attr('data-digit');
			
			
			if(dir=='up'){
				step[digit-1]=(step[digit-1]==9?0:++step[digit-1]);
				$('#digit'+digit+' div').css({'top':'-100%'});
				$('#digit'+digit+' div').removeClass('visible');
				$('#digit'+digit+' div:eq('+ step[digit-1]+')').addClass('visible');
				
			}
			else{
				
				step[digit-1]=(step[digit-1]>0?--step[digit-1]:9);
				$('#digit'+digit+' div').css({'top':'100%'});
				$('#digit'+digit+' div').removeClass('visible');
				$('#digit'+digit+' div:eq('+ step[digit-1]+')').addClass('visible');
				//console.log("else");
				
				
			}
		});

	
	
	/**********************/
	
	
	
	$('#menu').click(function(e) {
        if($(this).hasClass('open')){
			$(this).removeClass('open');
			$('#slidingMenu').slideUp('fast');
		}
		else{
			$(this).addClass('open');
			$('#slidingMenu').slideDown('fast');
		}
    });
	
	//////
	
	if($(document).width()>640){
        if ($('.upToDateSelfcare').length > 0 && $('.nearestStoreBox').length > 0) { 
    	shiftElement('.upToDateSelfcare','.nearestStoreBox',false);
	shiftElement('.needHelpVirginFamily','.upToDateSelfcare',true);
}

	}
	
	$('.flexslider').flexslider();

	
	$('#chatWithUsFixedBox').click(function(e){
		if($(this).hasClass('open')){
			$('#chatWithUsFixedBoxOpened').removeClass('on');
			$(this).removeClass('open');
		}
		else{
			$('#chatWithUsFixedBoxOpened').addClass('on');
			$(this).addClass('open');
		}
		
	});
	
	
	/*****************/
	$('.block').hover(
		function(e){
			$(this).addClass('hover');
		},
		function(e){
			$(this).removeClass('hover');	
		}
	);
	
	/****************/	
	
	$('.shareLink').click(function(e) {
		var overlay=$(this).attr('data-id')
		if($(this).hasClass('opened')){
			$(this).removeClass('opened');	
			$('#'+overlay+'Overlay').removeClass('on');
		}
		else{			
			$('#'+overlay+'Overlay').addClass('on');
			$(this).addClass('opened');
		}
    });


	/****************/
	
	initialize();
	

});// end of document.ready
/////////////////////////////
$( window ).resize(function() {
	
	if($(document).width()>640){
	shiftElement('.upToDateSelfcare','.nearestStoreBox',false);
	shiftElement('.needHelpVirginFamily','.upToDateSelfcare',true);
	}
});
//////////////////////////////

function shiftElement(elem,under,dependingOnFloatedDiv){
	var underBoxPos=$(under).position().top,
		underBoxHieght=$(under).height(),
		elemPos=$(elem).position().top,
		marginTop=elemPos-(underBoxPos+underBoxHieght)-15;
		
		if(dependingOnFloatedDiv){
			var underBoxPos=$('.nearestStoreBox').position().top,
				underBoxHieght=$('.nearestStoreBox').height(),
				underBoxHieght2=$('.upToDateSelfcare').height(),
				
				marginTop=elemPos-(underBoxPos+underBoxHieght+underBoxHieght2)-15;
	
		}
	
		/*console.log($(elem).attr('class'));
		console.log("under="+underBoxPos);
		console.log("underBoxHieght="+underBoxHieght);
		console.log("elemPos="+elemPos),
		console.log("marginTop="+marginTop);*/
		

		$(elem).css({"margin-top":(-1)*marginTop});
}


/////////////////////////////////////////
function initialize() {
	var map_canvas = document.getElementById('map');
	var elem=$('#map');
	if(elem.length>0){
		//alert('fvfvew');
		var mapOptions = {
		  center: new google.maps.LatLng(44.5403, -78.5463),
		  zoom: 12,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(map_canvas, mapOptions);
	}
}



