$(document).foundation({
	equalizer : {
		// Specify if Equalizer should make elements equal height once they become stacked.
		equalize_on_stack : false
	}
});
///////////////////////////////
var lang = $('html').attr("lang"), upToDateSelfcare = '';

jQuery(window).load(function() {
	//chooseNumberBoxH = jQuery('.chooseNumberBox').height();
	$(document).foundation({
		equalizer : {
			// Specify if Equalizer should make elements equal height once they become stacked.
			equalize_on_stack : false
		}
	});

	var v1 = $('.prepaidPostpaidList1').height(), v2 = $('.prepaidPostpaidList2').height();

	$('.prepaidPostpaidList1').height((v1 > v2 ? v1 : v2));

	if ($(document).width() > 640) {
		$('.howGetNumber .blockW').height($('.chooseUniqueNumberBox').height() - 70 + "px")
	}

	//$('.prepaidPostpaidList2').height();

	var $container = $('#contentBoxes');
	var isotopDir=(lang=="en"?true:false);
	// init
	$container.isotope({
		// options
		isOriginLeft: isotopDir,
		itemSelector : '.block',
		layoutMode : 'masonry'
	});

});
// end window.load

$(document).ready(function(e) {

	/**** Start Style Dropdown Menu in postpaid page****/
	if ($("select").length > 0) {
		$("select").uniform();
	}
	/**** End Style Dropdown Menu in postpaid page****/

	$('#sendLocation').click(function(e) {
		e.preventDefault();
		var storeLocationLat = $('#storeLocationLat').val();
		var storeLocationLong = $('#storeLocationLong').val();
		if (storeLocationLat != '' && storeLocationLong != '') {
			$('#location').slideDown("fast", function() {
				var $container = $('#contentBoxes');
				var isotopDir=(lang=="en"?true:false);
				// init
				$container.isotope({
					// options
					isOriginLeft: isotopDir,
					itemSelector : '.block',
					layoutMode : 'masonry'
				});
				
				Recaptcha.reload();

			});
		} else {
			alert(translate("Please select a store"));
		}
	});

	var $container = $('#contentBoxes');
	var isotopDir=(lang=="en"?true:false);
	// init
	$container.isotope({
		// options
		isOriginLeft: isotopDir,
		itemSelector : '.block',
		layoutMode : 'masonry'
	});

	if ($(document).width() <= 880) {
		$('.stickyTxt').each(function(index, element) {
			var src = '';
			src = $(this).attr('src').split('.png');

			$(this).attr('src', src[0] + "_mobile.png");

		});

	}

	$('.memberHelp li').not('.bookLinks .faqList li').click(function(e) {
		var question = $(this);
		if (question.hasClass('opened')) {
			question.removeClass('opened');
		} else {
			question.addClass('opened');
		}
		question.find('.answer').slideToggle("fast");
		// $container.isotope('layout');
		/* $container.isotope('reloadItems')*/
	});

	if ($('input[type="checkbox"]').length > 0) {
		$(":checkbox").uniform();
	}

	if ($('.upDown').length > 0) {
		$('.upDown').click(function(e) {
			e.preventDefault();
			var lnk = $(this);
			if (lnk.hasClass('opened')) {
				lnk.prev('.moreQuestions').slideUp("fast");
				lnk.removeClass('opened');
				lnk.find('span').text(lnk.attr('data-closed-link'));
			} else {
				lnk.prev('.moreQuestions').slideDown("fast");
				lnk.addClass('opened');
				lnk.find('span').text(lnk.attr('data-opened-link'));

			}

		});
	}

	/*var step=0;
	$(".spin").click(function(e) {

	var dir=$(this).attr('data-dir'),
	digit=$(this).attr('data-digit');

	if(dir=='up'){
	step=(step==9?0:++step);
	$('#digit'+digit+' div').removeClass('up');
	$('#digit'+digit+' div').removeClass('down');
	$('#digit'+digit+' div:eq('+ step+')').addClass('up');
	}
	else{
	$('#digit'+digit+' div').css({'top':'100%'});
	step=(step>=0?--step:9);
	$('#digit'+digit+' div').removeClass('down');
	$('#digit'+digit+' div').removeClass('up');
	$('#digit'+digit+' div:eq('+ step+')').addClass('down');

	}
	console.log('step='+step);
	/*$(".digit div.active + div").addClass('up');
	setTimeout(function(){
	$(".digit div.active").removeClass('active');
	$(".digit div.up").addClass('active').removeClass('up');

	},300);
	});
	*/
	///////

	/**********Cube**********/

	/**
	 * @param string x -The converted 7 digits phone Number Taken from the hidden field.
	 * @param [object] x0 -The Parent, used as param to keep the pointer on parent alive inside loops after killing parent everytime.
	 * @param int current -The current value after swiping.
	 * @param int AngleCounter -Keep the box container rotating on the desired angle , always multiplied by 90 degrees of rotation.
	 */

	if ($(".global-holder").length) {
		var x = $("#number").val().split("");
		$(".global-holder").each(function(index, element) {
			var x0 = $(this);
			var current, backN, nextN;
			if (x.length != 0) {
				current = (parseInt(x[index]));
				current == 0 ? nextN = 9 : nextN = current - 1;
				current == 9 ? backN = 0 : backN = current + 1;

				x0.find(".back").text(current - 1);
				x0.find(".current").text(current);
				x0.find(".next").text(current + 1);
			}
			var AngleCounter = 0;
			var filler = 0;
			x0.find('.nextBox').click(function() {
				$(".global-holder").removeClass("active3Dbox");
				x0.addClass("active3Dbox");
				//on this certian box
				AngleCounter += 1;
				current += 1;
				//console.log(current);
				x0.find(".back").addClass("blacker");
				x0.find(".next").removeClass("blacker");
				x0.find('.cube').stop().css({
					"-webkit-transform" : "rotateX(" + 90 * AngleCounter + "deg)",
					"transform" : "rotateX(" + 90 * AngleCounter + "deg)"
				});
				x0.find(".cube").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
					if (current > 9) {
						current = 0
					}
					x0.find('.cube').remove();
					x0.find('.counter-wrapper').append('<div class="cube"><div class="back blacker">' + ((current == 0) ? 9 : (current - 1)) + '</div><div class="current">' + current + '</div><div class="next blacker">' + ((current == 9) ? 0 : (current + 1)) + '</div><div class="fourth"></div></div>');
					AngleCounter = 0
				});

			});
			//callBack

			x0.find('.prevBox').click(function() {
				$(".global-holder").removeClass("active3Dbox");
				x0.addClass("active3Dbox");
				//on this certian box
				AngleCounter -= 1;
				current -= 1;

				x0.find(".next").addClass("blacker");
				x0.find(".back").removeClass("blacker");

				x0.find('.cube').stop().css({
					"-webkit-transform" : "rotateX(" + 90 * AngleCounter + "deg)",
					"transform" : "rotateX(" + 90 * AngleCounter + "deg)"
				});

				x0.find(".cube").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
					if (current < 0) {
						current = 9
					}
					x0.find('.cube').remove();
					x0.find('.counter-wrapper').append('<div class="cube"><div class="back blacker">' + ((current == 0) ? 9 : (current - 1)) + '</div><div class="current">' + current + '</div><div class="next blacker">' + ((current == 9) ? 0 : (current + 1)) + '</div><div class="fourth"></div></div>');
					AngleCounter = 0;
				});

			});
			//callBack

		});
		//each
	};//checking for box existence

	/***********End Of Cube Moderation****/

	/************Hide Show Top forms**************/

	/*$(".topUp").click(function(){
	$("#login-up,#join-up").slideUp(200);
	$("#top-up").slideToggle(400);
	$(".closeLink").show();
	});

	$(".loginUp").click(function(){
	$("#top-up,#join-up").slideUp(200);
	$("#login-up").slideToggle(400);
	$(".closeLink").show();
	});

	$(".joinUp").click(function(){
	$("#top-up,#login-up").slideUp(200);
	$("#join-up").slideToggle(400);
	$(".closeLink").show();
	});*/
	////////////////////////////////////
	$(".searchArea a:not(.searchArea .lang a)").click(function(e) {
		e.preventDefault();
		var popup = $(this).attr("data-popup");
		$('.top-forms-container').addClass('opened');

		if ($(this).hasClass('opened')) {
			$("#" + popup).slideUp(400);
			$(this).removeClass('opened');
		} else {
			$(".popup").slideUp(200);
			$(".searchArea a").removeClass('opened');
			$("#" + popup).slideDown(400);
			$(this).addClass('opened');
		}
	});
	////////////////////////////////////
	$(".closeLink").click(function() {
		$(".searchArea a").removeClass('opened');
		$(".popup").slideUp(400);
		$('.top-forms-container').removeClass('opened');
	});
	////////////////////////////////////

	$(document).on('mousedown', '.rangeslider', function() {
		var leftbubb = $(".rangeslider__handle").position().left;
		$("output.rangeprice").css("left", leftbubb - 107)
	});

	$(document).on('mouseenter', '.rangeslider, .rangeslider *', function() {
		var leftbubb = $(".rangeslider__handle").position().left;
		$("output.rangeprice").css("left", leftbubb - 107)
	});

	$(document).on('mouseleave', '.rangeslider, .rangeslider *', function() {
		var leftbubb = $(".rangeslider__handle").position().left;
		$("output.rangeprice").css("left", leftbubb - 107)
	});
	$(document).on('mouseup', '.rangeslider, .rangeslider *', function() {
		var leftbubb = $(".rangeslider__handle").position().left;
		$("output.rangeprice").css("left", leftbubb - 107)
	});
	$(document).on('mousemove', '.rangeslider, .rangeslider *', function() {
		var leftbubb = $(".rangeslider__handle").position().left;
		$("output.rangeprice").css("left", leftbubb - 107)
	});
	$(document).on('mouseout', '.rangeslider, .rangeslider *', function() {
		var leftbubb = $(".rangeslider__handle").position().left;
		$("output.rangeprice").css("left", leftbubb - 107)
	});
	$(document).on('mouseover', '.rangeslider, .rangeslider *', function() {
		var leftbubb = $(".rangeslider__handle").position().left;
		$("output.rangeprice").css("left", leftbubb - 107)
	});
	$(document).on('click', '.rangeslider, .rangeslider *', function() {
		var leftbubb = $(".rangeslider__handle").position().left;
		$("output.rangeprice").css("left", leftbubb - 107)
	});

	var step = [0, 0, 0, 0, 0, 0, 0];
	$(".spin").click(function(e) {
		$(".digit").removeClass("activated");
		$(this).parents(".digit-container").find(".digit").addClass("activated");

		var dir = $(this).attr('data-dir'), digit = $(this).attr('data-digit');

		if (dir == 'up') {
			step[digit - 1] = (step[digit - 1] == 9 ? 0 : ++step[digit - 1]);
			$('#digit' + digit + ' div').css({
				'top' : '-100%'
			});
			$('#digit' + digit + ' div').removeClass('visible');
			$('#digit' + digit + ' div:eq(' + step[digit - 1] + ')').addClass('visible');

		} else {

			step[digit - 1] = (step[digit - 1] > 0 ? --step[digit - 1] : 9);
			$('#digit' + digit + ' div').css({
				'top' : '100%'
			});
			$('#digit' + digit + ' div').removeClass('visible');
			$('#digit' + digit + ' div:eq(' + step[digit - 1] + ')').addClass('visible');
			//console.log("else");

		}
	});

	/**********************/

	$('#menu').click(function(e) {
		if ($(this).hasClass('open')) {
			$(this).removeClass('open');
			$('#slidingMenu').slideUp('fast');
		} else {
			$(this).addClass('open');
			$('#slidingMenu').slideDown('fast');
		}
	});

	//////

	$('.flexslider').flexslider();

	$('#chatWithUsFixedBox').click(function(e) {
		if ($(this).hasClass('open')) {
			$('#chatWithUsFixedBoxOpened').removeClass('on');
			$(this).removeClass('open');
		} else {
			$('#chatWithUsFixedBoxOpened').addClass('on');
			$(this).addClass('open');
		}

	});

	/*****************/
	$('.block').not('.prepaidPostpaid,.upToDateSelfcare').hover(function(e) {
		$(this).addClass('hover');
	}, function(e) {
		$(this).removeClass('hover');
	});

	$('.prepaid,.postpaid,.stayUpToDate').hover(function(e) {
		$(this).addClass('hover');
	}, function(e) {
		$(this).removeClass('hover');
	});

	/****************/

	$('.shareLink').click(function(e) {
		var overlay = $(this).attr('data-id');

		if ($(this).hasClass('opened')) {
			$(this).removeClass('opened');
			$('#' + overlay + 'Overlay').removeClass('on');
		} else {
			$('#' + overlay + 'Overlay').addClass('on');
			$(this).addClass('opened');
		}
	});

	/****************/

	initialize();

});
// end of document.ready
/////////////////////////////
$(window).resize(function() {

});
//////////////////////////////

/*function shiftElement(elem,under,dependingOnFloatedDiv){
var underBoxPos=$(under).position().top,
underBoxHieght=$(under).height(),
elemPos=$(elem).position().top,
marginTop=elemPos-(underBoxPos+underBoxHieght)-15;

if(dependingOnFloatedDiv){
var underBoxPos=$('.nearestStoreBox').position().top,
underBoxHieght=$('.nearestStoreBox').height(),
underBoxHieght2=$('.upToDateSelfcare').height(),

marginTop=elemPos-(underBoxPos+underBoxHieght+underBoxHieght2)-15;

}

$(elem).css({"margin-top":(-1)*marginTop});
}*/

/////////////////////////////////////////
function initialize() {
	var map_canvas = document.getElementById('map');
	var elem = $('#map');
	if (elem.length > 0) {
		//alert('fvfvew');
		var mapOptions = {
			center : new google.maps.LatLng(44.5403, -78.5463),
			zoom : 12,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(map_canvas, mapOptions);
	}
}

/////////////
